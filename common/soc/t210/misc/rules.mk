LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

GLOBAL_INCLUDES += \
	$(LOCAL_DIR)/../../../include \
	$(LOCAL_DIR)/../../../include/soc/$(TARGET) \
	$(LOCAL_DIR)/../../../include/drivers

MODULE_SRCS += \
	$(LOCAL_DIR)/misc.c

include make/module.mk

